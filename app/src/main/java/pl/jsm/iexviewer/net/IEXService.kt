package pl.jsm.iexviewer.net

import io.reactivex.Observable
import pl.jsm.iexviewer.data.models.QuoteResponse
import pl.jsm.iexviewer.data.models.Symbol
import retrofit2.http.GET
import retrofit2.http.Query

interface IEXService {

    @GET("stock/market/batch")
    fun getQuotes(@Query("symbols") symbols: String, @Query("types") types: String = "quote"): Observable<QuoteResponse>

    @GET("ref-data/symbols")
    fun getAvailableSymbols(): Observable<List<Symbol>>

}