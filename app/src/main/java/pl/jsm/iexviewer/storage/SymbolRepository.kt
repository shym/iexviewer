package pl.jsm.iexviewer.storage

import io.reactivex.Completable
import io.reactivex.Single
import pl.jsm.iexviewer.data.models.Symbol
import pl.jsm.iexviewer.db.SymbolDao
import javax.inject.Inject

class SymbolRepository @Inject constructor(private val symbolDao: SymbolDao) {

    fun getSavedSymbols(): Single<List<Symbol>> = symbolDao.getAll()

    fun saveSymbol(symbol: String): Completable {
        return Completable.fromAction {
            val size = getSavedSymbols()
                .blockingGet()
                .size
            val newSymbol = Symbol(symbol, size)
            symbolDao.insert(newSymbol)
        }
    }

    fun removeSymbol(symbol: String): Single<Int> = symbolDao.delete(symbol)

}