package pl.jsm.iexviewer.storage

import io.reactivex.Observable
import pl.jsm.iexviewer.data.models.QuoteResponse
import pl.jsm.iexviewer.net.IEXService
import javax.inject.Inject

class QuoteRepository @Inject constructor(private val iexService: IEXService) {

    fun getQuotes(names: String): Observable<QuoteResponse> {
        val handledNames = handleNames(names)
        return iexService.getQuotes(handledNames)
    }

    private fun handleNames(names: String): String {
        return names.split(" ").joinToString(separator = ", ")
    }

}