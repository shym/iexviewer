package pl.jsm.iexviewer.data.gson

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import pl.jsm.iexviewer.data.models.Quote
import pl.jsm.iexviewer.data.models.QuoteResponse
import java.lang.reflect.Type

class QuoteResponseDeserializer : JsonDeserializer<QuoteResponse> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): QuoteResponse {
        val quoteResponse = QuoteResponse(ArrayList())
        val quoteKey = "quote"
        json?.let {
            val gson = Gson()
            val jsonObject = it.asJsonObject
            for (key in jsonObject.keySet()) {
                val quote = gson.fromJson(jsonObject.getAsJsonObject(key).getAsJsonObject(quoteKey), Quote::class.java)
                quoteResponse.quotes.add(quote)
            }
        }

        return quoteResponse
    }
}