package pl.jsm.iexviewer.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Quote(
    @PrimaryKey var symbol: String,
    var companyName: String,
    var changePercent: Double,
    var latestPrice: Double,
    var order: Int = 99999
)