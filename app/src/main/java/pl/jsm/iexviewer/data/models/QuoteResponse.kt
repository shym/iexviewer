package pl.jsm.iexviewer.data.models

data class QuoteResponse(val quotes: ArrayList<Quote>)