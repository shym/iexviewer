package pl.jsm.iexviewer.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Symbol(@PrimaryKey var symbol: String, var order: Int)