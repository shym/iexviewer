package pl.jsm.iexviewer.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.jsm.iexviewer.ui.main.search.SearchFragment

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract fun bindSearchFragment(): SearchFragment

}