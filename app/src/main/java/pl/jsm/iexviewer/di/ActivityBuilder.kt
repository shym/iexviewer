package pl.jsm.iexviewer.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.jsm.iexviewer.ui.main.MainActivity

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

}