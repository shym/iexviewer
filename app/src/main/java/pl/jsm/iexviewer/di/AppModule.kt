package pl.jsm.iexviewer.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import pl.jsm.iexviewer.db.DbModule
import pl.jsm.iexviewer.net.NetModule
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class, NetModule::class, DbModule::class])
class AppModule {
    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application
}