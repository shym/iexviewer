package pl.jsm.iexviewer.ui.base

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import pl.jsm.iexviewer.R

abstract class BaseViewModel : ViewModel() {
    private val disposables = CompositeDisposable()

    protected fun registerDisposable(d: Disposable) {
        disposables.add(d)
    }

    private fun clearDisposables() {
        disposables.clear()
    }


    override fun onCleared() {
        clearDisposables()
        super.onCleared()
    }

    companion object {
        @JvmStatic
        @BindingAdapter("visibleIf")
        fun visibleIf(view: View, visible: Boolean) {
            view.visibility = if (visible) View.VISIBLE else View.GONE
        }

        @JvmStatic
        @BindingAdapter("resourceColor")
        fun setTextColor(view: TextView, color: Int) {
            view.setTextColor(view.context.getColor(color))
        }

        @JvmStatic
        @BindingAdapter("isFavourite")
        fun isFavourite(view: ImageView, isFavourite: Boolean) {
            view.setImageResource(if (isFavourite) R.drawable.ic_favorite_black_24dp else R.drawable.ic_favorite_border_black_24dp)
        }
    }
}