package pl.jsm.iexviewer.ui.main

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import io.reactivex.schedulers.Schedulers
import pl.jsm.iexviewer.data.models.Quote
import pl.jsm.iexviewer.data.models.QuoteResponse
import pl.jsm.iexviewer.storage.QuoteRepository
import pl.jsm.iexviewer.ui.base.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor(private val quoteRepository: QuoteRepository) : BaseViewModel() {

    val isLoading = ObservableBoolean(false)
    val foundQuotes = ObservableArrayList<Quote>()

    fun fetchQuotes(names: String) {
        isLoading.set(true)
        registerDisposable(
            quoteRepository.getQuotes(names)
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleSymbols, this::handleError)
        )
    }

    private fun handleSymbols(quoteResponse: QuoteResponse) {
        isLoading.set(false)
        foundQuotes.clear()
        foundQuotes.addAll(quoteResponse.quotes)
    }

    private fun handleError(error: Throwable) {
        isLoading.set(false)
    }
}