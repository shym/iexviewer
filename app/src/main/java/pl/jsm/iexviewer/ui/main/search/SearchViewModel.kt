package pl.jsm.iexviewer.ui.main.search

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import pl.jsm.iexviewer.data.models.Quote
import pl.jsm.iexviewer.storage.QuoteRepository
import pl.jsm.iexviewer.storage.SymbolRepository
import pl.jsm.iexviewer.ui.base.BaseViewModel
import pl.jsm.iexviewer.ui.base.SingleLiveEvent
import pl.jsm.iexviewer.ui.main.quote.QuoteRowViewModel
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val quoteRepository: QuoteRepository,
    private val symbolRepository: SymbolRepository
) : BaseViewModel() {

    val isLoading = ObservableBoolean(false)
    val foundQuotes = ObservableArrayList<QuoteRowViewModel>()
    val savedSymbols = HashSet<String>()
    val toggleSavouriteEvent = SingleLiveEvent<Quote>()

    init {
        fetchSavedSymbols()
    }

    fun fetchQuotes(names: String) {
        isLoading.set(true)
        registerDisposable(
            quoteRepository.getQuotes(names)
                .subscribeOn(Schedulers.io())
                .flatMap { Observable.fromIterable(it.quotes) }
                .map { QuoteRowViewModel(it, isQuoteFavourited(it), toggleSavouriteEvent) }
                .toList()
                .subscribe(this::handleSymbols, this::handleError)
        )
    }

    fun handleQuoteFavourited(quote: Quote) {
        if (isQuoteFavourited(quote)) {
            registerDisposable(
                symbolRepository.removeSymbol(quote.symbol)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe()
            )
            savedSymbols.remove(quote.symbol)
        } else {
            registerDisposable(
                symbolRepository.saveSymbol(quote.symbol)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe()
            )
            savedSymbols.add(quote.symbol)
        }
    }

    private fun handleSymbols(quotes: List<QuoteRowViewModel>) {
        isLoading.set(false)
        foundQuotes.clear()
        foundQuotes.addAll(quotes)
    }

    private fun handleError(error: Throwable) {
        isLoading.set(false)
    }

    private fun fetchSavedSymbols() {
        registerDisposable(
            symbolRepository.getSavedSymbols()
                .toObservable()
                .flatMap { Observable.fromIterable(it) }
                .map { it.symbol }
                .toList()
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleSavedSymbols)
        )
    }

    private fun handleSavedSymbols(symbols: List<String>) {
        savedSymbols.clear()
        savedSymbols.addAll(symbols)
    }


    private fun isQuoteFavourited(quote: Quote): Boolean = savedSymbols.contains(quote.symbol)
}