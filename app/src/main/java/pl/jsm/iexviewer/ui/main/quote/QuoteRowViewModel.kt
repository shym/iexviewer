package pl.jsm.iexviewer.ui.main.quote

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import pl.jsm.iexviewer.R
import pl.jsm.iexviewer.data.models.Quote
import pl.jsm.iexviewer.ui.base.BaseViewModel
import pl.jsm.iexviewer.ui.base.SingleLiveEvent
import java.math.RoundingMode
import java.text.DecimalFormat

class QuoteRowViewModel constructor(
    val quote: Quote,
    isFavourited: Boolean,
    private val toggleFavouriteEvent: SingleLiveEvent<Quote>
) : BaseViewModel() {

    val symbol = ObservableField<String>()
    val change = ObservableField<String>()
    val lastPrice = ObservableField<String>()
    val changeColor = ObservableField<Int>()
    val fullName = ObservableField<String>()
    val isFavourite = ObservableBoolean(false)

    init {
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING

        symbol.set(quote.symbol)
        change.set("${df.format(quote.changePercent)}%")
        lastPrice.set(df.format(quote.latestPrice))
        fullName.set(quote.companyName)
        changeColor.set(if (quote.changePercent > 0) R.color.quote_green else R.color.quote_red)
        isFavourite.set(isFavourited)
    }

    fun toggleFavourite() {
        toggleFavouriteEvent.value = quote
        isFavourite.set(!isFavourite.get())
    }

}