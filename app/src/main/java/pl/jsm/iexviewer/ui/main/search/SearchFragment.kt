package pl.jsm.iexviewer.ui.main.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.disposables.CompositeDisposable
import pl.jsm.iexviewer.R
import pl.jsm.iexviewer.databinding.FragmentSearchBinding
import pl.jsm.iexviewer.ui.base.BaseDaggerFragment
import pl.jsm.iexviewer.ui.main.quote.QuoteAdapter
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchFragment : BaseDaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val disposables = CompositeDisposable()

    private lateinit var quoteAdapter: QuoteAdapter
    private lateinit var binding: FragmentSearchBinding
    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        binding.model = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerForSearchEvents()
        registerForFavouritedEvent()
        initializeRecyclerView()
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    private fun registerForSearchEvents() {
        disposables.add(Observable.create(ObservableOnSubscribe<String> { subscriber ->
            binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    subscriber.onNext(query!!)
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    subscriber.onNext(newText!!)
                    return false
                }

            })
        })
            .map { text -> text.toLowerCase().trim() }
            .debounce(600, TimeUnit.MILLISECONDS)
            .distinct()
            .filter { text -> text.isNotBlank() }
            .subscribe(viewModel::fetchQuotes))
    }

    private fun initializeRecyclerView() {
        quoteAdapter = QuoteAdapter(viewModel.foundQuotes)
        binding.list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = quoteAdapter
        }
    }

    private fun registerForFavouritedEvent() {
        viewModel.toggleSavouriteEvent.observe(
            this,
            Observer { quote -> quote?.let { viewModel.handleQuoteFavourited(quote) } })
    }

}