package pl.jsm.iexviewer.ui.main

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import io.reactivex.disposables.CompositeDisposable
import pl.jsm.iexviewer.R
import pl.jsm.iexviewer.databinding.ActivityMainBinding
import pl.jsm.iexviewer.ui.base.BaseDaggerActivity
import pl.jsm.iexviewer.ui.main.search.SearchFragment
import javax.inject.Inject

class MainActivity : BaseDaggerActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var binding: ActivityMainBinding
    private val disposables = CompositeDisposable()

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializeBinding()
        showSearchFragment()
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    private fun initializeBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.model = viewModel
    }

    private fun showSearchFragment() {
        val ft = supportFragmentManager.beginTransaction().replace(R.id.fragment_container, SearchFragment())
        ft.commit()
    }

}
