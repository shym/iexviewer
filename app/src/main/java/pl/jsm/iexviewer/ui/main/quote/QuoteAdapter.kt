package pl.jsm.iexviewer.ui.main.quote

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import pl.jsm.iexviewer.R
import pl.jsm.iexviewer.databinding.RowQuoteBinding


class QuoteAdapter constructor(val items: List<QuoteRowViewModel>) : RecyclerView.Adapter<QuoteAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: RowQuoteBinding = DataBindingUtil.inflate(inflater, R.layout.row_quote, parent, false)
        return ItemViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(items[position])
    }

    class ItemViewHolder(private val binding: RowQuoteBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: QuoteRowViewModel) {
            binding.model = item
            binding.executePendingBindings()
        }

    }
}