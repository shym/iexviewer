package pl.jsm.iexviewer.db

import androidx.room.Database
import androidx.room.RoomDatabase
import pl.jsm.iexviewer.data.models.Symbol

@Database(entities = [Symbol::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun symbolDao(): SymbolDao
}