package pl.jsm.iexviewer.db

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Single
import pl.jsm.iexviewer.data.models.Symbol

@Dao
interface SymbolDao {

    @Query("SELECT * FROM symbol")
    fun getAll(): Single<List<Symbol>>

    @Query("SELECT * FROM symbol WHERE symbol LIKE :name")
    fun findByName(name: String): Single<List<Symbol>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(symbols: List<Symbol>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(symbol: Symbol)

    @Delete
    fun delete(symbol: Symbol)

    @Query("DELETE FROM symbol WHERE symbol = :symbol")
    fun delete(symbol: String): Single<Int>

}